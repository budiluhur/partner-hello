# partner-hello


##Steps to create CI/CD deploy

- Create jenkins container

````
docker run  -u root --restart on-failure -p 8990:8080 -p 50000:50000 -v /Users/andry.sunandar01/jenkins_home:/var/jenkins_home -v $(which docker):/usr/bin/docker -v /var/run/docker.sock:/var/run/docker.sock jenkins/jenkins:lts
````

- Create SCM pipeline from jenkins to build jar, build docker image and push to nexus



##Create kubernetes cluster

````
cd k8s-vagrant
vagrant up
````

####use "vagrant ssh" to test the cluster


